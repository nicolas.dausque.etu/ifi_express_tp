## Tips 1

Si vous ne voulez pas ctrl-c à chaque changement de votre application :

```bash
$ npm install nodemon --save
$ nodemon yourApp.js
```

## Tips 2

Pour obtenir les paramètres d'un GET dans une requête

```js
app.get('/', function(req, res){
  res.send('id: ' + req.query.id);
});
```